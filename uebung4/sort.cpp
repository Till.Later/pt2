#include <iostream>
#include <vector>
#include <list>
#include <string>
#include <algorithm>
#include <cassert>

// function template for printing contents of containers to std::cout
template<class T>
void printContainer(T& container)
{
  std::cout << "{";
  for (auto element : container)
    std::cout << element << " ";
  std::cout << "}" << std::endl;
}

// Todo 4.4 - Merge the given lists [leftIt..midIt) and [midIt..rightIt)
template<class T>
void merge(T leftIt, T midIt, T rightIt)
{
  assert(leftIt <= midIt && midIt <= rightIt);

  std::vector<typename T::value_type> mergedValues(rightIt - leftIt);
  auto iterator1 = leftIt;
  auto iterator2 = midIt;
  auto newListIterator = mergedValues.begin();

  while (iterator1 < midIt && iterator2 < rightIt) {
    if (*iterator2 < *iterator1)
      *(newListIterator++) = *(iterator2++);
    else
      *(newListIterator++) = *(iterator1++);
  }

  while (iterator1 < midIt)
    *(newListIterator++) = *(iterator1++);

  newListIterator = mergedValues.begin();
  while (leftIt < iterator2)
    *(leftIt++) = *(newListIterator++);

}

// Todo 4.4 - Sort the given container using merge sort.
template<class T>
void mergeSort(T leftIt, T rightIt)
{
  assert(leftIt < rightIt);
  if (std::next(leftIt) < rightIt) {
    auto midIt = leftIt + (rightIt - leftIt) / 2;
    mergeSort(leftIt, midIt);
    mergeSort(midIt, rightIt);

    merge(leftIt, midIt, rightIt);
  }
}

int main(int argc, char** argv)
{
  // define sample data
  std::vector<int> sampleDataInt = { 10, 1, 12, 33, 24, 5, 6, -7, -2, 19 };
  std::vector<std::string> sampleDataString = { "Die", "eines", "ist", "Gebrauch", "der", "Sprache", "in", "sein", "Wortes", "Bedeutung" };

  // test for integer vector
  printContainer(sampleDataInt);
  mergeSort(sampleDataInt.begin(), sampleDataInt.end());
  printContainer(sampleDataInt);

  // test for string vector

  printContainer(sampleDataString);
  mergeSort(sampleDataString.begin(), sampleDataString.end());
  printContainer(sampleDataString);

  return 0;
}
