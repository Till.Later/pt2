#include <iostream>
#include <cstring>
#include <ctime>
#include "bitmap_image.hpp"
#include <cassert>

struct Raster {
    Raster(int w, int h) : width(w), height(h)
    {
      data = new int[width*height];
    }

    Raster(int w, int h, float seedProbability) : width(w), height(h)
    {
      data = new int[width*height];

      // Todo 4.1a: Fill randomly
      for (int i = 0; i < width*height; i++)
        data[i] = ((std::rand() % 100) < 100 * seedProbability) ? 1 : 0; // Probability of inserting value 1 is seedProbability, otherwise value is 0
    }

    Raster(const std::string &filename)
    {
      bitmap_image image(filename);

      if (!image)
      {
        std::cerr << "Could not open bitmap!" << std::endl;
      }

      height = image.height();
      width = image.width();

      data = new int[width*height];

      // Todo 4.1a: Load image by using image.get_pixel
      unsigned char red;
      unsigned char green;
      unsigned char blue;

      for (int h = 0; h < height; h++)
        for (int w = 0; w < width; w++ ) {
          image.get_pixel(w, h, red, green, blue);
          // A black pixel represents 1, all other values represent 0
          data[h * width + w] = (red || blue || green) ? 0 : 1;
        }
    }

    void save(const std::string &filename)
    {
      // Todo 4.1a: Save image by using image.set_pixel
      // Living cells should be stored as black pixels, all other as white ones.
      bitmap_image image(width, height);

      for (int h = 0; h < height; h++)
        for (int w = 0; w < width; w++) {
          if (data[h * width + w]) image.set_pixel(w, h, 0, 0, 0);
          else image.set_pixel(w, h, 255, 255, 255);
        }

      image.save_image(filename);
    }

    ~Raster()
    {
      delete[] data;
    }

    int width;
    int height;
    int* data;
};

// This struct parses all necessary command line parameters. It is already complete and doesn't have to be modified.
// However - feel free to add support for additional arguments if you like.
struct CommandLineParameter
{
    CommandLineParameter(int argc, char* argv[])
            : width(0)
            , height(0)
            , invasionFactor(0)
            , isTorus(false)
            , maxIterations(200)
    {
      if (argc % 2 == 0)
      {
        std::cerr << "Missing value for " << argv[argc - 1] << std::endl;
        argc--;
      }

      for (int i = 1; i < argc; i += 2)
      {
        if (!strcmp(argv[i], "-w"))
        {
          width = atoi(argv[i + 1]);
        }
        else if (!strcmp(argv[i], "-h"))
        {
          height = atoi(argv[i + 1]);
        }
        else if (!strcmp(argv[i], "-s"))
        {
          seedProbability = static_cast<float>(atof(argv[i + 1]));
        }
        else if (!strcmp(argv[i], "-p"))
        {
          patternFilename = argv[i + 1];
        }
        else if (!strcmp(argv[i], "-o"))
        {
          outputDirectory = argv[i + 1];
        }
        else if (!strcmp(argv[i], "-iv"))
        {
          invasionFactor = static_cast<float>(atof(argv[i + 1]));
        }
        else if (!strcmp(argv[i], "-t"))
        {
          isTorus = strcmp(argv[i + 1], "0") != 0;
        }
        else if (!strcmp(argv[i], "-i"))
        {
          maxIterations = atoi(argv[i + 1]);
        }
      }

      if ((width != 0 || height != 0) && !patternFilename.empty())
      {
        std::cout << "Width and height are ignored, because pattern is defined." << std::endl;
      }

      if (width < 0 || height < 0)
      {
        std::cerr << "Width or height has a invalid value." << std::endl;
        width = 0;
        height = 0;
      }
    }

    int width;
    int height;
    float seedProbability;
    std::string patternFilename;
    std::string outputDirectory;
    float invasionFactor;
    bool isTorus;
    int maxIterations;
};

int neighborValue(const Raster &raster, int x, int y, bool isTorus)
{
  // Todo 4.1b: Return number of living neighbors.
  // In case isTorus is false and (x, y) is outside of raster, return 0
  // In case isTorus is true and (x, y) is outside of raster use value of matching cell of opposite side

  // ensure that the function has been called with a cell that is in the range of our field
  assert(!(x >= raster.width || x < 0 || y >= raster.height || y < 0));

  int livingNeighbours = 0;

  for (int h = -1; h <= 1; h++)
    for (int w = -1; w <= 1; w++) {
      // if the cell we check is the cell we want to know the neighbors from, then we skip it.
      if (!(h || w)) continue;
      int neighbourX = x + w;
      int neighbourY = y + h;

      // Set the correct cell indices for the borders in case of a torus.
      if (isTorus) {
        if (neighbourX >= raster.width) neighbourX = 0;
        else if (neighbourX < 0) neighbourX = raster.width - 1;

        if (neighbourY >= raster.height) neighbourY = 0;
        else if (neighbourY < 0) neighbourY = raster.height - 1;
      }
      // If torus is not enabled, cell indices might be out of range. Skip them.
      if (neighbourX < 0 || neighbourY < 0 || neighbourX >= raster.width || neighbourY >= raster.height) continue;

      // Sum up amount of living neighbors.
      livingNeighbours += raster.data[(neighbourY * raster.width) + neighbourX];
    }

  return livingNeighbours;
}

void simulateInvasion(Raster &raster, float invasionFactor)
{
  if (invasionFactor <= 0)
    return;

  // Todo 4.1c: Flip random some cells (probability to flip for each cell is invasionFactor)
  for (int i = 0; i < raster.height*raster.width; i++)
    if ((std::rand() % 100) < 100 * invasionFactor)
      raster.data[i] = 1 - raster.data[i];
}

void simulateNextState(Raster &raster, bool isTorus)
{
  // Todo 4.1b: Play one iteration of Game of Life
  // Initialize temp array with the next state of the field.
  int nextState[raster.width*raster.height];

  // Calculate for each cell, if it has to be dead or alive in the next state. Store the result in the nextState array.
  for (int y = 0; y < raster.height; y++)
    for (int x = 0; x < raster.width; x++) {
      int aliveNeighbors = neighborValue(raster, x, y, isTorus);
      int cellState = raster.data[y * raster.width + x];
      nextState[y * raster.width + x] = ((cellState == 0 && aliveNeighbors == 3) || (cellState == 1 && (aliveNeighbors == 2 || aliveNeighbors == 3))) ? 1 : 0;
    }

  // Copy the next state into the raster.
  for(int i = 0; i < raster.width*raster.height; i++)
    raster.data[i] = nextState[i];
}

int main(int argc, char* argv[])
{
  Raster* raster = nullptr;

  // Todo 4.1a: Initialize random seed
  std::srand(std::time(0));

  CommandLineParameter cmd(argc, argv);
  if (!cmd.patternFilename.empty())
  {
    raster = new Raster(cmd.patternFilename);
  }
  else
  {
    raster = new Raster(cmd.width, cmd.height, cmd.seedProbability);
  }

  for (int iteration = 0; iteration <= cmd.maxIterations; iteration++)
  {
    raster->save(cmd.outputDirectory + "game_of_life_" + std::to_string(iteration) + ".bmp");
    simulateInvasion(*raster, cmd.invasionFactor);
    simulateNextState(*raster, cmd.isTorus);
  }

  delete raster;

  return 0;
}