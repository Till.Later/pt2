#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <iomanip>
#include <map>
#include <vector>
#include <algorithm>
#include <numeric>
#include <chrono>

struct Route
{
    int airlineId;
    int sourceId;
    int destinationId;
};

bool operator<(const Route& r1, const Route& r2) {
  return r1.destinationId < r2.destinationId;
}

void importRoutesData(char* path, std::vector<Route>& routes)
{
  std::cout << "Importing routes data.." << std::endl;
  std::ifstream file(path);
  std::string field, line;

  while (std::getline(file, line))
  {
    std::istringstream linestream;
    linestream.str(line);
    int fieldNum = 0;
    Route route;
    route.airlineId = route.sourceId = route.destinationId = -1;

    while (std::getline(linestream, field, ';'))
    {
      try
      {
        switch (fieldNum)
        {
          case 1: // airline id
            route.airlineId = std::stoi(field);
            break;
          case 3: // source id
            route.sourceId = std::stoi(field);
            break;
          case 5: // dest id
            route.destinationId = std::stoi(field);
            break;
          default:
            break;
        }
      }
      catch (const std::invalid_argument&)
      {
        //std::cout << "Couldn't convert field " << currentLineNum << " correctly (invalid argument)!" << std::endl;
        //std::cout << field << std::endl;
      }
      catch (const std::out_of_range&)
      {
        //std::cout << "Couldn't convert field " << currentLineNum << " correctly (out of range)!" << std::endl;
        //std::cout << field << std::endl;
      }

      fieldNum++;
    }

    if (route.airlineId > -1 && route.sourceId > -1 && route.destinationId > -1)
      routes.push_back(route);
  }
}

// Todo 4.3a - Return the number of routes for the given destination id based on a linear search. Count the number of lookups.
int linearSearch(int destID, std::vector<Route>& routes, long long& numLookups)
{
  int numRoutes = 0;
  for (auto &route : routes) {
    numLookups++;
    if (route.destinationId == destID) numRoutes++;
  }
  return numRoutes;
}

// Todo 4.3a - Evaluate the linearSearch function by calling it for every possible destination id (1..9541).
// Return the number of lookups and the processing time as a pair of long longs.
// Use std::chrono for time measurement.
std::pair<long long, long long> evaluateLinearSearch(std::vector<Route>& routes)
{
  long long numLookups = 0;
  long long duration = 0;
  std::chrono::time_point<std::chrono::system_clock> start, end;
  start = std::chrono::system_clock::now();

  for (int i = 1; i <= 9541; i++) {
    linearSearch(i, routes, numLookups);
  }

  end = std::chrono::system_clock::now();
  duration = std::chrono::duration_cast<std::chrono::microseconds> (end-start).count();

  return std::make_pair(numLookups, duration);
}

// Todo 4.3b - Return the number of routes for the given destination id based on a binary search. Count the number of lookups.
// The vector should have been sorted before calling this function.
int binarySearch(int destID, std::vector<Route>& routes, long long& numLookups)
{
  auto right = routes.end();
  auto left = routes.begin();
  auto middle = left + (right - left)/2;

  int middleId = middle->destinationId;
  numLookups++;

  while (middleId != destID) {
    if ((right - left) < 2)
      return 0;

    if (middleId < destID) {
      left = middle;
      middle = middle + (right - middle)/2;
    } else {
      right = middle;
      middle = left + (middle - left)/2;
    }

    middleId = middle->destinationId;
    numLookups++;
  }

  // After this loop we know for sure that the element that the middle points to has the destinationId we are looking for.
  int sum = 0;

  // Count the routes on the left side of the middle
  auto destinationCheck = middle;
  numLookups++;
  while(destinationCheck->destinationId == destID && destinationCheck >= routes.begin()){
    numLookups++;
    destinationCheck--;
    sum++;
  }

  // Count the routes on the right side of the middle
  destinationCheck = middle + 1;
  numLookups++;
  while(destinationCheck->destinationId == destID && destinationCheck < routes.end()){
    numLookups++;
    destinationCheck++;
    sum++;
  }

  return sum;

}

// Todo 4.3b - Evaluate the binarySearch function by calling it for every possible destination id (1..9541).
// Return the number of lookups and the processing time as a pair of long longs.
// Use std::chrono for time measurement.
// Attention: sorting is *not* part of the evaluation and should be conducted beforehand.
std::pair<long long, long long> evaluateBinarySearch(std::vector<Route>& routes)
{
  long long numLookups = 0;
  long long duration = 0;

  std::chrono::time_point<std::chrono::system_clock> start, end;

  start = std::chrono::system_clock::now();
  for(int i = 1; i <= 9541; i++){
    binarySearch(i, routes, numLookups);
  }
  end = std::chrono::system_clock::now();

  duration = std::chrono::duration_cast<std::chrono::microseconds> (end-start).count();

  return std::make_pair(numLookups, duration);
}

int main(int argc, char * argv[])
{
  if(argc != 2)
  {
    std::cout << "not enough arguments - USAGE: sort [ROUTE DATASET]" << std::endl;
    return -1;  // invalid number of parameters
  }

  static std::vector<Route> routes;

  std::cout << "Given path to routes.csv: " << argv[1] << std::endl;

  importRoutesData(argv[1], routes);

  auto result = evaluateLinearSearch(routes);
  std::cout << result.first << " - " << result.second << std::endl;

  // Pre-sort our routes-list to benefit from binary search
  std::sort(routes.begin(), routes.end());
  result = evaluateBinarySearch(routes);
  std::cout << result.first << " - " << result.second << std::endl;
  
  /*********************************** Betrachtung der Zugriffs- und Laufzeitzahlen ***********************************
   * Die Zugriffszahlen bei der linearen Suche sind sehr hoch. Da die lineare Suche nicht von einer sortierten Liste
   * profitieren kann, muss, um alle gesuchten destinationIDs zu finden und zu summieren, jedes Element aufgerufen werden.
   * Nachdem im Import alle ungültigen Zeilen entfernt wurden, verbleiben 66.548 Routen, die durchsucht werden müssen.
   * Da die Suche für alle 9.541 destinationIDs durchgeführt wird, erfolgen 66.548 * 9.541 = 634.934.468 Zugriffe auf das
   * Array. In unserem Durchlauf ergab das eine Gesamtlaufzeit von 4,05986 Sekunden (4.059.860 microseconds).
   *
   * Die binäre Suche profitiert von der sortierten Liste, indem sie sich an das/die gesuchte/n Element/e mit der
   * destinationIDs in einer Art Halbierungsverfahren annähert. Sobald ein Element der geuschten destinationID gefunden
   * wurde, kann der Algorithmus von der sortierten Liste profitieren, da alle gleichen destinationIDs auf jeden Fall
   * links und rechts von der bereits gefundenen destinationID liegen. Ab diesem Zeitpunkt übernimmt in unserer
   * Implementation ein lineares Verfahren, was zur Konsequenz hat, dass in einer theoretischen Routenliste mit nur
   * einer einzigen destinationID direkt beim ersten Anlauf die binäre Suche fündig wird. Das lineare Verfahren, dass
   * nun die Ränder ermittelt, kommt erst zum Schluss, wenn es an den äußersten Rändern der Liste angelangt ist, was
   * bedeutet, dass schließlich doch die gesamte Liste linear durchlaufen wird. Ein besseres Verfahren würde durch
   * stetiges verdoppeln auch in einem binären Annäherungsverfahren die Ränder ermitteln. Nichtsdestotrotz bringt die
   * binäre Suche in ihrer jetzigen Implementierung enorme Laufzeitvorteile mit sich. Es erfolgen lediglich
   * 221.578 Zugriffe auf das Array. In unserem Durchlauf ergab dieses Verfahren eine Laufzeit
   * von 0,009039 Sekunden (9.039 microseconds). Grob überschlagen bedeutet dass, dass die binäre Suche im Bereich
   * von einem Tausendstel der Laufzeit gegenüber der linearen Suche liegt.
  *********************************** Betrachtung der Zugriffs- und Laufzeitzahlen ************************************/

  return 0;
}
