#include <cassert>
#include <iostream>
#include <string>
#include <cstddef>
#include <vector>
#include <cstring>
#include <cmath>
#include <map>

static const auto N = 8;

auto A = std::vector<int>();
auto B = std::vector<int>();
auto C = std::vector<int>();

// Create a constant map to bind each staple-vector to it's number and make it easier to access and modify elements.
// This could also have been defined at each function call, but for efficiency reasons it's defined here.
const std::map<int, std::vector<int>& > towerMap = {{0, A}, {1, B}, {2, C}};

void print() {
  #ifdef _WIN32
  std::system("cls");
  #else
  std::system("clear");
  #endif

  // Todo 5.2: Print current state
  std::cout << std::endl;

  // Create lambda functions for printing some standard outputs for the towers.
  auto black =    [ ](int a) { for (int i = 0; i < a; i++) std::cout << "\u2588"; };
  auto white =    [ ](int a) { for (int i = 0; i < a; i++) std::cout << " "; };
  auto emptyrow = [&](int a) { for (int i = 0; i < a; i++) { white(2 * N + 4); black(1); white(2 * N + 5); }};

  // Print the towers.
  for (int i = N-1; i >= 0; i--) {
    // Between each plate print an empty space.
    emptyrow(3);
    std::cout << std::endl;

    for (int j = 0; j < 3; j++) {
      if (i >= towerMap.at(j).size())
        // If the current staple has no plate at this position, print an empty space.
        emptyrow(1);
      else {
        // Print black and white characters according to the width of the current plate of the current stable.
        int width = towerMap.at(j).at((unsigned)i);
        int margin = (N - width);
        white(4 + 2 * margin);
        black(1 + 4 * width);
        white(5 + 2 * margin);
      }
    }
    std::cout << std::endl;
  }
  emptyrow(3);
  std::cout << std::endl;

  // Print bottom line
  for (int i = 0; i < 3; i++) {
    black(4 * N + 9); white(1);
  }
  std::cout << std::endl;

  // Print bottom line with stable name
  for (int i = 0; i < 3; i++) {
    black(2 * N + 4); putchar('A' + i); black(2 * N + 4); white(1);
  }
  std::cout << std::endl << std::endl;
}

void ToH(const int n, const int a, const int b, const int c, int & moves)
{
  // Todo 5.2: Implement ToH and print
  if (n == 1) {
    // Switch plate from stable a to staple c.
    towerMap.at(c).push_back((int)towerMap.at(a).back());
    towerMap.at(a).pop_back();

    // Print the towers.
    print();
    // Print the last move information.
    std::cout << "Last move: " << (char)('A' + a) << " to " << (char)('A' + c) << std::endl;

    // Count moves.
    moves++;

    // Wait for user input.
    getchar();

  } else {
    ToH(n - 1, a, c, b, moves);
    ToH(1, a, b, c, moves);
    ToH(n - 1, b, a, c, moves);
  }
}

int main(int argc, char ** argv)
{
  int moves = 0;

  for (int i = N; i > 0; --i)
    A.push_back(i);

  print();
  getchar();

  ToH(N, 0, 1, 2, moves);
  std::cout << "minimal number of moves: " << moves << std::endl;

  getchar();
  return 0;
}
