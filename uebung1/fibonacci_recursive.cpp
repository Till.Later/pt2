#include <cstdlib>
#include <iostream>

long fibonacciCounter = 0;
int fibonacci(int number) {
	// ToDo: Exercise 2.c - count number of calculation steps
	fibonacciCounter++;
	// ToDo: Exercise 2.a - return 0 on bad arguments
	if ( number < 1 ) return 0;
	// ToDo: Exercise 2.a - retrieve nth fibonacci number recursively
	if (number <= 2) return 1;
	else return fibonacci( number - 1 ) + fibonacci( number - 2 );
}

int main(int argc, char * argv[]) {
	if(argc != 2)
		return 1;	// invalid number of parameters

	int n = std::atoi(argv[1]);

	// ToDo: Exercise 2.c - print calculation steps
    if (n < 0 || n > 46) {
		// Ist der Eingabewert ungültig, findet keine Berechnung statt und es wird frühzeitig 0 zurückgegeben.
		std::cout << 0;
		// Die main-Methode gibt eine 1 zurück um zu indizieren, dass der Aufruf fehlerhaft war.
		return 1;
	}
	else std::cout << n << " : " << fibonacci(n) << " : #" << fibonacciCounter << std::endl;
	return 0;
}
