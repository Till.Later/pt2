#include <cstdlib>
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#define COINSLENGTH 12

void change(const int due, const int paid, std::ostream& out){
    // ToDo: return set of change tuples
    int returnMoney = paid - due;
    int coins[COINSLENGTH] = {5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5, 2, 1};


    out << "coin, num" << std::endl;
    for(int i = 0; i < COINSLENGTH; i++){
        int amount = (int)(returnMoney / coins[i]);
        if (amount != 0) {
            out << coins[i] << "," << amount << std::endl;
        }
        returnMoney = returnMoney % coins[i];
    }
}

int main(int argc, char * argv[]) {
    if(!(argc == 3 || argc == 5)) {
        std::cerr << "Invalid number of parameters" << std::endl;
        return 1;    // invalid number of parameters
    }

    const int due  = std::atoi(argv[1]);
    const int paid = std::atoi(argv[2]);

    if(due <= 0 || paid < due) {
        std::cerr << "Invalid input values!" << std::endl;
        return 1;
    }
    if (argc == 3) {
        // ToDo: print change data as CSV to console
        change(due, paid, std::cout);
    } else if (argv[3] == std::string("-o")){
        // ToDo: Write output into file (.txt)
        std::fstream fs;
        fs.open(argv[4] + std::string(".txt"), std::fstream::out);
        change(due, paid, fs);
        fs.close();
    } else {
        std::cerr << "Unknown option: " << argv[3] << std::endl;
    }
    return 0;
}
