
#include <cstdlib>
#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>


int triangular(int number) {
	// ToDo: Exercise 1.a - return triangular number
	return ( number * ( number + 1 ) ) / 2;
}

int getMaximumTriangular() {
    // Wir verwenden die Lösungsformel um die Gleichung zur Berechnung der Dreieckszahl aufzulösen
    // und herauszufinden bei welcher Eingabe der maximale signed integer value überschritten wird.
    // Da die Nullstellenberechnung mit der Lösungsformel immer 2 Lösungen bestimmt muss die größere Lösung ermittelt werden.
    int x = (int) (-1 / 2 + sqrt(0.25 + std::numeric_limits<int>::max()));
    int y = (int) (-1 / 2 - sqrt(0.25 + std::numeric_limits<int>::max()));
	return x > y ? x : y;
}

// Unsere Implementation der Funktion pretty_print formartiert eine Dezimalzahl, indem sie die die Dezimaltrennzeichen
// einfügt und die Zahl direkt ausgibt (wie in 1.c gefordert "...entsprechend formatiert auf der Konsole ausgeben soll.")
void pretty_print(int decimal) {
	// ToDo: Exercise 1.c - print number with decimal marks to console
    if (decimal > 1000) {
        pretty_print( (decimal - ( decimal % 1000 ) ) / 1000);
        std::cout << "." << std::setfill('0') << std::setw(3) << decimal % 1000;
    } else {
        std::cout << decimal % 1000;
    }
}

int main(int argc, char * argv[]) {
    if (argc != 2)
        return 1;    // invalid number of parameters

    int n = std::atoi(argv[1]);

    // ToDo: Exercise 1.b - check if n is in domain, if not print valid domain and codomain and return 2
    if (n < 1 || n > getMaximumTriangular()) {
        std::cout << "domain = [";
        pretty_print(1);
        std::cout << ";";
        pretty_print(getMaximumTriangular());
        std::cout << "], codomain = [";
        pretty_print(triangular(1));
        std::cout << ";";
        pretty_print(triangular(getMaximumTriangular()));
        std::cout << "]" << std::endl;
		return 1;
    }
    // ToDo: Exercise 1.a - print triangular number to console
    pretty_print(triangular(n));
	return 0;
}
