#include <cstdlib>
#include <iostream>
#include <limits>

int fibonacciCounter = 0;
int fibonacci(int number){
	// ToDo: Exercise 2.b - return 0 on bad arguments
	if (number < 1) return 0;

	// ToDo: Exercise 2.b - retrieve nth fibonacci number iteratively
	int i = 1, previous = 1, preprevious = 0, t;
	while (i < number) {
		// ToDo: Exercise 2.c - count number of calculation steps
        // nur ein vollständiger Durchlauf der while-Schleife erhöht den counter um 1.
        // Die Berechnung der ersten Fibonacci-Zahl erfordet dementsprechend keinen einzigen Berechnungsschritt.
		fibonacciCounter++;
		t = previous + preprevious;
        preprevious = previous;
        previous = t;
		i++;

		// Sobald die Summe von preprevious und previous größer ist, als der Maximalwert für einen signed int,
        // wird abgebrochen und 0 zurückgegeben.
		if ((std::numeric_limits<int>::max() - preprevious) - previous < 0) {
            return 0;
        }
	}

	return previous;
}

int main(int argc, char * argv[]) {
	//std::cout << std::numeric_limits<int>::max() << std::endl;
	if(argc != 2)
		return 1;	// invalid number of parameters

	int n = std::atoi(argv[1]);

	// ToDo: Exercise 2.c - print calculation steps
	std::cout << n << " : "  <<fibonacci(n) << " : #" << fibonacciCounter << std::endl;
	return 0;
}
