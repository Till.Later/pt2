
#include <cstdlib>
#include <iostream>


// ToDo: Exercise 2.d - adapt and implement combinations
unsigned long long combinations(int number) {
    // number entspricht der Anzahl an vertikalen Stufen
    // Das Betreten eines Bürgersteiges entspricht dementsprechend einer einzigen Stufe.
    // Für die zwei Stufen ergeben sich zwei Möglichkeiten, entweder einen großen, oder zwei kleine Schritte.
    // d.h. 1 Stufe => 2. Fibonacci-Zahl  2 Stufen => 3. Fibonacci-Zahl
    if (number < 1) return 0;

    unsigned long long i = 1, previous = 1, preprevious = 0, t;
    while (i <= number) {
        // ToDo: Exercise 2.c - count number of calculation steps
        t = previous + preprevious;
        preprevious = previous;
        previous = t;
        i++;

        // Sobald die Summe von preprevious und previous größer ist, als der Maximalwert für einen signed int,
        // wird abgebrochen und 0 zurückgegeben.
        if ((std::numeric_limits<unsigned long long>::max() - preprevious) - previous < 0) {
            return 0;
        }
    }
    return previous;
}

int main(int argc, char * argv[])
{
	if(argc != 2) {
        return 1;	// invalid number of parameters
    }

	int n = std::atoi(argv[1]);
    std::cout << std::numeric_limits<unsigned long long>::max() << std::endl;
	std::cout << combinations(n);

	return 0;
}
