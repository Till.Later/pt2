#include <iostream>
#include <cassert>
#include <list>
#include <cstdlib>
#include <string>

using namespace std;


class Account {
public:
    Account(long accountNo, double balance, double limit, bool locked);
    ~Account();

    long accountNo() const { return accountNo_; }

    virtual void setLimit(double limit) { limit_ = limit; }
    double getLimit() const { return limit_; }

    virtual bool credit(double amount);
    virtual bool debit(double amount);
    double balance() const { return balance_; }

    void setLock(bool locked) { locked_ = locked; }
    bool getLock() const { return locked_; }

private:
    long accountNo_;
    double balance_;
    double limit_;
    bool locked_;
};

Account::Account(long accountNo, double balance, double limit, bool locked) :
        accountNo_{accountNo},
        balance_{balance},
        limit_(limit),
        locked_{locked}
{}

Account::~Account() {}

bool Account::credit(double amount)
{
  assert(amount>=0.0);

  // cannot use locked account
  if(locked_) return false;

  balance_ = balance_ + amount;
  return true;
}

bool Account::debit(double amount)
{
  assert(amount>=0.0);

  // cannot use locked account
  if(locked_) return false;

  // check if limit is hit
  if(balance_ - amount < limit_) return false;

  // change balance
  balance_ = balance_ - amount;
  return true;
}

typedef std::list<std::pair<std::string, double>> TransactionProtocol;

class LoggedAccount : public Account {
private:
    TransactionProtocol transactionProtocol_;

    void updateBalance() {
      std::prev(transactionProtocol_.end())->second = balance();
    }

    void logTransaction (std::string action, double amount) {
      transactionProtocol_.insert(std::prev(transactionProtocol_.end()), std::make_pair(action, amount));
    }

public:
    LoggedAccount(long accountNo, double balance, double limit, bool locked) : Account(accountNo, balance, limit, locked) {
      transactionProtocol_.emplace_back("**initial balance**", balance);
      transactionProtocol_.emplace_back("**current balance**", balance);
    }

    virtual bool credit(double amount) override {
      // Perform parent class action and store result
      bool result = Account::credit(amount);
      if (result) {
        // Log transaction if it is valid and has proceeded
        logTransaction("credit", amount);
        // update **current balance**
        updateBalance();
      }
      return result;
    }

    virtual bool debit(double amount) override {
      // Perform parent class action and store result
      bool result = Account::debit(amount);
      if (result) {
        // Log transaction if it is valid and has proceeded
        logTransaction("debit", amount);
        // update **current balance**
        updateBalance();
      }
      return result;
    }

    virtual void setLimit(double limit) override {
      // Log limit set
      logTransaction("setLimit", limit);
      // Perform parent class action
      Account::setLimit(limit);
      return;
    }

    TransactionProtocol transactions() const {
      return transactionProtocol_;
    }

};

int main(int argc, char** argv)
{
  Account A1(19920, 0.0, -1000.0, false);
  LoggedAccount A2(20020, 0.0, -1000.0, false);

  A1.credit(500.0);
  A2.credit(500.0);
  A2.debit(100.0);
  A2.setLimit(-2000.);

  for (const auto& x : A2.transactions())
  {
    cout << x.first << ": " << x.second << endl;
  }

  return 0;
}

