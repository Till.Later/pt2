#include <iostream>
#include <cassert>
#include <cmath>

#define delta 0.01

class Amount {
	// Todo 6.2
	// Implement class Amount
public:
  // Constants
  static constexpr float exchangeRate = 1.135;
  static constexpr float normalTax = 1.19;
  static constexpr float reducedTax = 1.07;

  // Enums
  enum tax {NORMAL, REDUCED};
  enum currency {EUR, USD};

  // Ctr's
  Amount(float net = 0, currency cur = EUR, tax tax = NORMAL, std::string id = "") : net_{net}, currency_{cur}, tax_{tax}, identifier_{id} {};
  Amount(float net, tax tax, std::string id = "") : Amount{net, EUR, tax, id} {};
  Amount(float net, currency cur, std::string id) : Amount{net, cur, NORMAL, id} {};
  Amount(float net, std::string id) : Amount{net, EUR, NORMAL, id} {};
  Amount(currency cur, tax tax, std::string id = "") : Amount{0, cur, tax, id} {};
  // wir hoffen, das ist "bequem" genug?!

  // Getter
  float getNetAmount() const { return net_; }
  float getGrossAmount() const { return net_ * ( (tax_ == NORMAL) ? Amount::normalTax : Amount::reducedTax ); }
  std::string getIdentifier() const { return identifier_; }
  currency getCurrency() const { return currency_; }
  tax getTax() const { return tax_; }

  // Setter
  void setTax(tax tax) { tax_ = tax; }
  void setNetAmount(float net) { net_ = net; }
  void setIdentifier(std::string newIdentifier) { identifier_ = newIdentifier; }

  // Further functions
  void setCurrency(currency cur) {
    net_ = net_ * ( (cur == currency_) ? 1 : (cur == EUR) ? 1/Amount::exchangeRate : Amount::exchangeRate );
    currency_ = cur;
  }

private:
  float net_;
  currency currency_;
  tax tax_;
  std::string identifier_;

};

void test()
{
	// Todo 6.2
	// Implement tests
  Amount a{};
  Amount b{15, Amount::currency::USD, Amount::tax::REDUCED, "Pink Fluffy Unicorn"};
  Amount c{10, Amount::currency::USD};

  // Simply by calling the constructors, we test them.
  Amount d{14.2F, Amount::tax::REDUCED};
  Amount e{99999999.99F, Amount::currency::EUR, "Hasso's Account"};
  Amount f{1234, "Max Mustermann"};
  Amount g{Amount::currency::USD, Amount::tax::REDUCED, "Pink Fluffy Unicorn - again"};


  a.setNetAmount(50);
  assert(std::abs(a.getGrossAmount() - 59.5) < delta);
  assert(a.getTax() == Amount::tax::NORMAL);
  a.setCurrency(Amount::currency::USD);
  a.setTax(Amount::tax::REDUCED);
  assert(std::abs(a.getNetAmount() - 56.75) < delta && std::abs(a.getGrossAmount() - 60.7225) < delta);
  assert(a.getCurrency() == Amount::currency::USD);
  assert(a.getTax() == Amount::tax::REDUCED);
  a.setCurrency(Amount::currency::EUR);
  assert(std::abs(a.getNetAmount() - 50) < delta && std::abs(a.getGrossAmount() - 53.5) < delta);
  a.setIdentifier("The Great Narwhal");
  assert(a.getIdentifier() == "The Great Narwhal");


  assert(std::abs(b.getNetAmount() - 15) < delta);
  assert(std::abs(b.getGrossAmount() - 16.05) < delta);
  assert(b.getTax() == Amount::tax::REDUCED);
  assert(b.getCurrency() == Amount::currency::USD);
  assert(b.getIdentifier() == "Pink Fluffy Unicorn");

  assert(std::abs(c.getNetAmount() - 10) < delta);
  assert(std::abs(c.getGrossAmount() - 11.9) < delta);
  assert(c.getTax() == Amount::tax::NORMAL);
  assert(c.getCurrency() == Amount::currency::USD);
  assert(c.getIdentifier() == "");

  std::cout << "All tests worked fine!" << std::endl;
}

int main()
{
	test();
	return 0;
}
