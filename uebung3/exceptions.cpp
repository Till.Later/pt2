#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cfloat>
#include <iomanip>
#include <vector>
#include <ctime>

// transforms a string to a date. Throws a logic_error if year is *not* between 2005 and 2015
std::tm stringToTime(std::string date)
{
	std::tm t;
#if defined(__GNUC__) && (__GNUC__ < 5)
	strptime(date.c_str(), "%d.%m.%Y", &t);
#else
	std::istringstream ss(date);
	ss >> std::get_time(&t, "%d.%m.%Y");
#endif

	if(t.tm_year < 105 || t.tm_year > 115)
		throw std::logic_error("Year should be between 2005 and 2015");

	return t;
}

struct FormatException
{
	int m_actLine;
	std::string m_actFields;
};

void parseLine(std::string line, int lineNum)
{
  // TODO 3.1b: parse a given line, check dates by calling stringToTime, check temperature/rainfall by calling std::stof.
  // Catch all exceptions thrown by these methods.
  // If there have been any exceptions, aggregate all necessary information into an instance of FormatException and throw that instance.

	const std::string fieldNames[3] = { "Date", "Temperature", "Rainfall" };
  FormatException exception {-1};

  std::istringstream ss;
  ss.str(line);
  std::vector<std::string>dataArray(3);

  for(unsigned int i = 0; i < dataArray.size(); i++) {
    std::getline(ss, dataArray[i], ';');
  }

  for(int fieldNumber = 0; fieldNumber < dataArray.size(); fieldNumber++) {
    try {
      switch (fieldNumber) {
        case 0:
          stringToTime(dataArray[0]);
          break;
        case 1:
        case 2:
          std::stof(dataArray[fieldNumber]);
          break;
      }
    }
    catch (std::logic_error &fail) {
      // Write data into the exception. Append the new field name if there is more than one error found.
      exception.m_actLine = lineNum;
      exception.m_actFields = exception.m_actFields + fieldNames[fieldNumber] + " ";
    }
  }

  // Throw the exception if an error was found.
  if(exception.m_actLine != -1){
    // Remove last space
    exception.m_actFields = exception.m_actFields.substr(0, exception.m_actFields.size()-1);
    throw exception;
  }

  return;
}

// TODO 3.1d
void writeOutFormatException(const FormatException& e)
{
  //Create new output file if does not exist and write text or append if it's already exist.
  try {
    std::ofstream errorstream;
    errorstream.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    errorstream.open(std::string("weather_babelsberg.log"), std::ofstream::app);
    errorstream << "Failure on line " << e.m_actLine << ", field(s): " << e.m_actFields << std::endl;
    errorstream.close();
  } catch (std::ofstream::failure &fail) {
    std::cerr << "Exception writing file: " << fail.what() << std::endl;
  }

  // Print Error to console
  std::cout << "Failure on line " << e.m_actLine << ", field(s): " << e.m_actFields << std::endl;
  return;
}

void checkData(std::string path)
{
  // TODO 3.1a: open file + read each line + call parseLine function (catch ifstream::failure)
  // TODO 3.1c: read each line + call parseLine function (catch FormatException) + count valid + invalid lines

  //Create a new output file - delete the old one if there is one.
  //We have to do this here, because if not we create by multiple program starts multiple outputs in the same file.
  try {
    std::ofstream errorstream;
    errorstream.exceptions(std::ofstream::failbit | std::ofstream::badbit);
    errorstream.open(std::string("weather_babelsberg.log"), std::ofstream::out);
    errorstream.close();
  } catch (std::ofstream::failure &fail) {
    std::cerr << "Error: " << fail.what() << std::endl;
  }

  int validLines = 0;
	int invalidLines = 0;
	std::ifstream file;
  file.exceptions(std::ofstream::failbit | std::ofstream::badbit);

  std::string line;

  try {
    //Open file
    file.open(path, std::ifstream::in);
    //Skip the first line (table head).
    std::getline(file, line);

    while (file.good() && !file.eof() && file.is_open()) {
      std::getline(file, line);
      try {
        //Check the line and throw an exception if necessary.
        // +1 : get the correct line number temporary, but we don't know if it is a valid or invalid line.
        // +1 : we skip the table head and start with line 2. 
        parseLine(line, validLines + invalidLines + 2);
        validLines++;
      }
      catch (FormatException &fail) {
        //Handle the FormatException of a line error
        writeOutFormatException(fail);
        invalidLines++;
      }
    }
    file.close();
  }
  catch (const std::ifstream::failure& fail) {
    std::cerr << "Exception opening/reading file: " << fail.what() << std::endl;
  }
  catch (std::ios_base::failure &fail) {
    std::cerr << "Exception: " << fail.what() << std::endl;
  }

	std::cout << "valid data fields: " << validLines << " - invalid data fields: " << invalidLines << std::endl;
}

int main(int argc, char * argv[])
{
	if(argc != 2)
	{
		std::cout << "Invalid number of arguments - USAGE: exceptions [DATASET]" << std::endl;
		return -1;
	}

	checkData(argv[1]);

	return 0;
}
