#include <iostream>
#include <assert.h>
#include <vector>
#include <map>

using namespace std;

/**************** Note ****************
 * We decided to choose a way between performance optimization and memory usage. (Also just the performance was
 * part of the task) That's why we have to use the (ordered) map instead of the unordered map which would bring
 * us a performance improvement. We discussed this decision and decided because we don't know what data type the
 * objects and stamps (especially memory size) are and we didn't want to store them twice for this reason.
 * We know that the fastest way would be to use unordered map and store everything twice instead of ordered maps
 * and pointer linking.
 **************** Note ****************/

template<class T, class stamp_type = unsigned long>
class StampedSet {
/**************** Condition for the stamp_type ****************
 * stamp_type operator++<stamp_type s>;
 * stamp_type operator=<int i>;
 **************** Condition for the stamp_type ****************/

public:
StampedSet() {}

stamp_type add(const T& obj) {
  stamp_type newNextStamp = nextStamp();
  unsigned long numberOfObjs = noOfObjects();
  unsigned long numberOfStamps = noOfStamps();

  lastInserted_++;

  if (keyToStampPointerArray_.find(obj) == keyToStampPointerArray_.end()) {
    keyToStampPointerArray_[obj] = std::vector<stamp_type *>{};
  }

  stampToKeyPointer_[newNextStamp] = &(const_cast<T&>(keyToStampPointerArray_.find(obj)->first));
  keyToStampPointerArray_[obj].push_back(&(const_cast<stamp_type&>(stampToKeyPointer_.find(newNextStamp)->first)));

  assert(newNextStamp + 1 == lastInserted_);
  assert(numberOfObjs == noOfObjects() || numberOfObjs + 1 == noOfObjects());
  assert(numberOfStamps + 1 == noOfStamps());
  assert(keyToStampPointerArray_[obj].size() >= 1);

  return newNextStamp;
}

void erase(const T& obj) {
  assert(containsObject(obj));
  for (const stamp_type *s : keyToStampPointerArray_[obj]) {
    stampToKeyPointer_.erase(*s);
    assert(!containsStamp(*s));
  }

  keyToStampPointerArray_.erase(obj);
  assert(!containsObject(obj));
}

unsigned long noOfObjects() const {
  return keyToStampPointerArray_.size();
}

unsigned long noOfStamps() const {
  unsigned long amount = stampToKeyPointer_.size();
  assert(amount - noOfObjects() >=0);
  return amount;
}

bool containsObject(const T& obj) const {
  return keyToStampPointerArray_.find(obj) != keyToStampPointerArray_.end();
}

bool containsStamp(const stamp_type& s) const {
  return stampToKeyPointer_.find(s) != stampToKeyPointer_.end();
}

const T& findObject(const stamp_type& s) const {
  assert(containsStamp(s));
  return *(stampToKeyPointer_.find(s)->second);
}

stamp_type lastStamp(const T& obj) const {
  assert(containsObject(obj));
  return *(keyToStampPointerArray_.find(obj)->second.back());
}

stamp_type firstStamp(const T& obj) const {
  assert(containsObject(obj));
  return **(keyToStampPointerArray_.find(obj)->second.begin());
}

stamp_type nextStamp() const {
  return lastInserted_;
}

template<typename L>
void process(const T& obj, L&& fct) {
  assert(containsObject(obj));
  auto sIt = keyToStampPointerArray_[obj].begin();
  while (sIt != keyToStampPointerArray_[obj].end()) {
    if (fct(obj, **sIt)) {
      stampToKeyPointer_.erase(**sIt);
      sIt = keyToStampPointerArray_[obj].erase(sIt);
    } else {
      sIt++;
    }
  }

  if (!keyToStampPointerArray_[obj].size()) add(obj);
  assert(keyToStampPointerArray_[obj].size() >= 1);
}

template<typename L>
void process(stamp_type from, stamp_type to, L&& fct) {
  std::vector<T*> toAddObj{};
  for (stamp_type s = from; s <= to; s++) {
    auto objIt = stampToKeyPointer_.find(s);
    if (objIt == stampToKeyPointer_.end()) continue;
    T obj = *(objIt->second);
    if (fct(obj, s)) {
      toAddObj.push_back(objIt->second);
      keyToStampPointerArray_[obj].erase(
              std::remove_if(keyToStampPointerArray_[obj].begin(),
                             keyToStampPointerArray_[obj].end(),
                             [s](stamp_type *stamp){return *stamp == s;}),
              keyToStampPointerArray_[obj].end());
      stampToKeyPointer_.erase(s);
      assert(!containsStamp(s));
    }
  }
  for (T *obj : toAddObj) if (!keyToStampPointerArray_[*obj].size()) add(*obj);
  assert(stampToKeyPointer_.size() - noOfObjects() >=0);
}

stamp_type restart(const T& obj) {
  assert(containsObject(obj));
  erase(obj);
  stamp_type s = add(obj);
  assert(containsStamp(s));
  assert(s == nextStamp() - 1);
  assert(keyToStampPointerArray_[obj].size() == 1);
  return s;
}

private:
std::map<T, std::vector<stamp_type *>> keyToStampPointerArray_;
std::map<stamp_type, T *> stampToKeyPointer_;
stamp_type lastInserted_ = 0;
};


void test() {
StampedSet<float, int> sfs;
float f {0.0f};
int s {0};
int z {0};

// check add, containsObject, containsStamp
f = 3.14f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 9.99f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 5.01f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 3.14f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 9.99f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 3.14f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 3.14f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 1.11f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));
f = 1.11f; s = sfs.add(f); assert(sfs.containsStamp(s)); assert(sfs.containsObject(f));

// test process by lambda that prints contents
sfs.process(0, sfs.nextStamp(), [](float f, int i){ cout << "(" << f << ", " << i << ") "; return false; }); cout << endl;

// check erase
f = 8.12f; s = sfs.add(f); sfs.erase(f); assert(!sfs.containsStamp(s)); assert(!sfs.containsObject(f));

// test assignment of next stamp to next added object
z = sfs.nextStamp(); f = 2.22f; s = sfs.add(f); assert(s == z);

// test restart
f = 2.22f; sfs.add(f); sfs.add(f); z = sfs.nextStamp(); s = sfs.restart(f); assert(s == z);

// check first/last stamps
f = 2.33f; s = sfs.add(f); assert(s == sfs.firstStamp(f)); assert(s == sfs.lastStamp(f));
f = 2.44f; s = sfs.add(f); z = sfs.add(f); assert(s == sfs.firstStamp(f)); assert(z == sfs.lastStamp(f));

// check find object
f = 2.55f; s = sfs.add(f); assert(f == sfs.findObject(s));
int N {0};
sfs.process(0, 100, [&N](float f, int i){ N++; return false; }); cout << "N: " << N << ", stamps: " << sfs.noOfStamps() << endl; assert(N == sfs.noOfStamps());

// test process deletion
f = 3.14f; sfs.process(f, [](float f, int i){ return true; }); assert(sfs.lastStamp(f) == 18);
sfs.process(1, 7, [](float f, int i){ return f < 6.0f; }); assert(sfs.lastStamp(5.01f) == 19); assert(sfs.firstStamp(9.99f) == 1); assert(sfs.firstStamp(1.11f) == 8);

// check different instantiations
StampedSet<string, int> sss;
StampedSet<char, unsigned long> scs;
}

int main(int,char**) {
  test();
  return 0;
}
