#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <regex>

bool isValueCorrect(const std::string &teststring, const int &column)
{
	std::regex regExp;

	switch (column) {
		// Todo: implement cases for other columns
		case 5:
			regExp = "^(\"([A-Z0-9]{3,4}|)\")?$";
			break;
		case 8:
			regExp = "^([1-2][0-9]{0,4}|[1-9][0-9]{0,3}|0)$";
			break;
		case 10:
			regExp = "^\"(E|U|N|O|Z|A|S)\"$";
			break;
		default:
			regExp = "^.*$";
			break;
	}

	return std::regex_match(teststring, regExp);
}

void readTokensAndLines(char* path)
{
	std::fstream errorstream;
	errorstream.open(std::string("fileio.log"), std::fstream::out);

	std::cout << path << std::endl;
	std::fstream file(path);
	std::string parsed, line;

	while (std::getline(file, line)) {
		std::stringstream linestream;
		linestream.str(line);

		int i = 0;
		while (std::getline(linestream, parsed, ';')) {
			// Anmerkung: sollten in einer Zeile zwei oder mehr Fehler gefunden werden, so wird die Zeile mehrfach in
      // die Datei geschrieben. Dies ist jedoch beim aktuellen Datensatz nicht der Fall.
			if (!isValueCorrect(parsed, i)) {
				errorstream << line << " => Error in field: " << i << std::endl;
			}

			switch (i) {
				case 1:
					parsed = parsed.substr(1);
          parsed.pop_back();
          std::cout << parsed;
          break;
				case 11:
					parsed = parsed.substr(1);
          parsed.pop_back();
          std::cout << " - " << parsed;
          break;
				default:
					break;
			}
			i++;
		}
		std::cout << std::endl;

		// Todo: - Split line and write result to std::cout
		//       - Check each part of line with isValueCorrect and log if values are not supported
		//       - Use and extend isValueCorrect function for this

		//errorstream.close(); - http://stackoverflow.com/questions/4802494/do-i-need-to-close-a-stdfstream
	}
}

int main(int argc, char * argv[])
{
	// Wir verwenden den Datensatz, in dem die Einträge durch Semikola getrennt sind.
	if(argc != 2)
	{
		std::cout << "not enough arguments - USAGE: fileio [DATASET]" << std::endl;
		return -1; // invalid number of parameters
	}

	std::cout << "Given path to airports.dat: " << argv[1] << std::endl;
	readTokensAndLines(argv[1]);

	return 0;
}
