#include <iostream>
#include <string>
#include <vector>
#include <sstream>

std::string step(std::string state, int value)
{
  // Todo: Handle all possible states and values and return the respective new state
  // Note: Accepted coin values are {10, 20, 50}.
  if(value == 10 || value == 20 || value == 50){
    switch(value){
      case 10:
        if(state == "0") return "10";
        if(state == "10") return "20";
        if(state == "20") return "30";
        if(state == "30") return "40";
      case 20:
        if(state == "0") return "20";
        if(state == "10") return "30";
        if(state == "20") return "40";
      default:
        return "dispense";
    }
  }
    // for all states and inputs which are not handled above show a message and return original state
    std::cout << "Invalid input.\n";
    return state;
}


std::string stepExtended(std::string state, int value)
{
  if(state == "0"){
    switch(value){
      case 1: return "1-0";
      case 2: return "2-0";
      case 3: return "3-0";
      default: return "0";
    }
  }

  if(value == 10 || value == 20 || value == 50){
    switch(value){
      case 10:
        if(state == "1-0") return "1-10";
        if(state == "2-0") return "2-10";
        if(state == "3-0") return "3-10";
        if(state == "1-10") return "1-20";
        if(state == "2-10") return "2-20";
        if(state == "3-10") return "3-20";
        if(state == "1-20") return "1-30";
        if(state == "2-20") return "2-30";
        if(state == "3-20") return "3-30";
        if(state == "1-30") return "1-40";
        if(state == "2-30") return "2-40";
        if(state == "3-30") return "3-40";
        if(state == "2-40") return "2-50";
        if(state == "3-40") return "3-50";
        if(state == "2-50") return "2-60";
        if(state == "3-50") return "3-60";
        if(state == "2-60") return "2-70";
        if(state == "3-60") return "3-70";
        if(state == "3-70") return "3-80";
        if(state == "3-80") return "3-90";
      case 20:
        if(state == "1-0") return "1-20";
        if(state == "2-0") return "2-20";
        if(state == "3-0") return "3-20";
        if(state == "1-10") return "1-30";
        if(state == "2-10") return "2-30";
        if(state == "3-10") return "3-30";
        if(state == "1-20") return "1-40";
        if(state == "2-20") return "2-40";
        if(state == "3-20") return "3-40";
        if(state == "2-30") return "2-50";
        if(state == "3-30") return "3-50";
        if(state == "2-40") return "2-60";
        if(state == "3-40") return "3-60";
        if(state == "2-50") return "2-70";
        if(state == "3-50") return "3-70";
        if(state == "3-60") return "3-80";
        if(state == "3-70") return "3-90";
      case 50:
        if(state == "2-0") return "2-50";
        if(state == "3-0") return "3-50";
        if(state == "2-10") return "2-60";
        if(state == "3-10") return "3-60";
        if(state == "2-20") return "2-70";
        if(state == "3-20") return "3-70";
        if(state == "3-30") return "3-80";
        if(state == "3-40") return "3-90";
      default:
        return "dispense";
    }
  }

    // for all states and inputs which are not handled above show a message and return original state
    std::cout << "Invalid input.\n";
    return state;
}

int main(int argc, char * argv[])
{
  std::string state = "0";
  bool extendedMode = false;

  if(argc == 2)
    extendedMode = true;

  while(true)
  {
    int value = 0;
    std::string input;
    if(state == "0" && extendedMode)
      std::cout << "Current state: " << state << ". Please select a drink (1, 2, 3): ";
    else
      std::cout << "Current state: " << state << ". Please input a coin (10, 20, 50): ";
    std::getline(std::cin, input);
    std::stringstream(input) >> value;

    state = extendedMode ? stepExtended(state, value) : step(state, value);

    if(state == "dispense")
    {
      std::cout << "Here is your drink. Goodbye!\n\n";
      state = "0";
    }
  }
}
